#include "example.h"

double add_numbers(const double f1, const double f2) { return f1 + f2; }

double subtract_numbers(const double f1, const double f2) { return f1 - f2; }

double multiply_numbers(const double f1, const double f2) { return f1 * f2; }

void BankAccount::deposit(int amount) { this->balance += amount; }
bool BankAccount::withdraw(int amount) {
    if (this->balance >= amount) {
        this->balance -= amount;
        return true;
    }
    return false;
}
