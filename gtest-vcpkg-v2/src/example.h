#pragma once

double add_numbers(const double f1, const double f2);

double subtract_numbers(const double f1, const double f2);

double multiply_numbers(const double f1, const double f2);

struct BankAccount {
    int balance = 0;
    BankAccount() {}
    explicit BankAccount(int balance) : balance{balance} {}
    void deposit(int amount);
    bool withdraw(int amount);
};
