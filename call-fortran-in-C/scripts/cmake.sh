#!/usr/bin/env bash

function banner() {
   echo "cmake tooling"
   echo " use this script to initialize and build project"
}

function usage() {
   echo "USAGE:"
   echo "   $(basename $0) [-h] (init|build|clean|install)"
}

function checker() {
   echo "Current directory: $(pwd)"
   if [ ! -d $(pwd)/scripts ]; then
      echo "Run script from directory: '$(
         cd $(dirname $0)/..
         pwd -P
      )'"
      exit 2
   fi
   if [ ! -f $(pwd)/scripts/$(basename $0) ]; then
      echo "Run script from directory: '$(
         cd $(dirname $0)/..
         pwd -P
      )'"
      exit 2
   fi
}

function init() {
   if [ ! -d build ]; then
      mkdir build
   fi
   cd build
   cmake .. \
      "-DCMAKE_TOOLCHAIN_FILE=$(eval echo ~$USER)/Git/vcpkg/scripts/buildsystems/vcpkg.cmake" \
      -DUSE_CHEMKIN=ON -DUSE_LSODE=ON
   echo "Run 'bash scripts/$(basename $0) build' to build"
}

function build() {
   if [ ! -d build ]; then
      echo "build directory doesn't exist"
      echo "Run 'bash scripts/$(basename $0) init' to initialize"
      exit 2
   fi
   cd build
   cmake --build .
}

function clean() {
   if [ ! -d build ]; then
      echo "build directory doesn't exist"
      echo "Run 'bash scripts/$(basename $0) init' to initialize"
      exit 2
   fi
   rm -rf build
}

function install() {
   echo "installing..."
}

checker
case $1 in
init)
   init
   ;;
build)
   build
   ;;
clean)
   clean
   ;;
install)
   install
   ;;
-h | --help | "")
   banner
   usage
   ;;
*)
   echo "Command not found: $1"
   usage
   exit 2
   ;;
esac
